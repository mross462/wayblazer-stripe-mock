* DevOps Engineer Exercise

Cloned From git@github.com:Cogniteinc/stripe-mock.git

** Development Environment
Add configuration to this project such that developers can use docker-compose to build, run, restart, and teardown a local dev environment. (Assuming they have Docker installed and configured correctly). See the README in the project for how to run the app.

`docker-compose build
docker-compose run test
docker-compose up
`

** Continuous Integration
Configure a build system that you are familiar with such that when commits are made to master:
The tests in the project are run. [here](https://bitbucket.org/mross462/wayblazer-stripe-mock/src/7ab501621025640c9a8cb7667b5e551582ca1e3d/.circleci/config.yml?at=master&fileviewer=file-view-default#config.yml-31)
An image is built [here](https://bitbucket.org/mross462/wayblazer-stripe-mock/src/7ab501621025640c9a8cb7667b5e551582ca1e3d/.circleci/config.yml?at=master&fileviewer=file-view-default#config.yml-24:27)
An image is run and an endpoint is tested for successful response. (Integration Test) [here](https://bitbucket.org/mross462/wayblazer-stripe-mock/src/7ab501621025640c9a8cb7667b5e551582ca1e3d/.circleci/config.yml?at=master&fileviewer=file-view-default#config.yml-38)
The image is pushed to a repository [here](https://bitbucket.org/mross462/wayblazer-stripe-mock/src/7ab501621025640c9a8cb7667b5e551582ca1e3d/.circleci/config.yml?at=master&fileviewer=file-view-default#config.yml-49:53)
The image is deployed to a runtime environment (See next step) [here](https://bitbucket.org/mross462/wayblazer-stripe-mock/src/7ab501621025640c9a8cb7667b5e551582ca1e3d/.circleci/config.yml?at=master&fileviewer=file-view-default#config.yml-49:53)
Any other branch should simply build and test. [here](https://bitbucket.org/mross462/wayblazer-stripe-mock/src/16aad095c03ab54447fb2cfcc737f5b81ae3d0e2/.circleci/config.yml?at=master&fileviewer=file-view-default#config.yml-6:15)

** Continuous Deployment and Hosting
Choose a method to host the docker images in “production” in AWS. There’s no right answer and all design decisions are subject to the triangle, of course. Be prepared to defend your choices for Docker hosting. Your CI system should be designed to deploy your code to this environment.

I chose aws Fargate to host the service which is hosted here behind an elb:

curl -i http://waybl-Publi-10A0GOTTGA05W-570425862.us-east-1.elb.amazonaws.com/v1/charges -H "Authorization: Bearer sk_test_123"

I had never used it and wanted to experiment on the setup. Per the page:

`AWS Fargate is a technology for Amazon ECS and EKS* that allows you to run containers without having to manage servers or clusters. With AWS Fargate, you no longer have to provision, configure, and scale clusters of virtual machines to run containers.
`

For a project like this I thought it prudent, but cost longterm might become a problem as with any AWS product.

I have some concerns about implementing health checks with this fargate and the service, and the current the service replaces containers (meaning downtime) without rotating them in individually (an improvement to make, or a bug on the AWS side).

My attempt was implemented at /status, but I was receiving 401s when the TargetGroup was rotating tasks in and out of the LB.

If I purchased a domain I would setup SSL and a route 53 domain name for the service.

I'm not going to include a diagram.

The general setup can be found in the aws/cloudformation directory and following the order of creating the following stacks:

fargate_vpc.yaml
repo.yaml
service.yaml

Before implementing the CI, create the stacks in that order.

*** What are some of the security concerns of this implementation?

No HTTPS.
Auth Isn't Real Auth.
There are no IAM roles preventing anything, but it is not a very permissive app.
I should probably lock down the sg to my home and the wayblazer office so the network traffic is blocked.

*** What could be implemented to improve it?

That is documented throughout this README.

Handled by AWS, and hopefully later by a wallet, if not by a team.

*** How would you handle keeping Secrets (sensitive environment variables required by the application)? Why?

Ideally this should be done in KMS or Vault.

*** Describe in detail what you would monitor about this application in production and how (in detail).

This is not a robust app. Monitor traffic on the ELB, and the subnets and using VPC Flow logs any suspicious activity forwarded to a cloudwatch log.

Send:
Your github repo fork containing your build automation, README, and any configuration as code.
https://bitbucket.org/mross462/wayblazer-stripe-mock
Either a link to your build system, or instructions to replicate your build system behavior.

Create the VPC then the, ECR repo, and then import the project into circleci.

I set this up in circleci, checkout and add the project, but make sure to add aws env vars to be able to do the cloudformation deploy for the service.

Also I could probably figure out a better way of handling test and deploy, but I'm a bit unfamiliar with workspaces in circleci, this was my first time using it (as is evident in the commit log)

Usage of the cloudformation templates is documented in them.

Thanks for looking.
